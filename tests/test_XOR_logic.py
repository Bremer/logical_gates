from logical_gates import logical_gates

"""The following code represents the logical XOR-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : True
    True  | False : True
    True  | True  : False """


def test_XOR_false_false():
    assert logical_gates.xor_logic(False, False) is False


def test_XOR_false_true():
    assert logical_gates.xor_logic(False, True) is True


def test_XOR_true_false():
    assert logical_gates.xor_logic(True, False) is True


def test_XOR_true_true():
    assert logical_gates.xor_logic(True, True) is False


def test_XOR_None_1():
    assert logical_gates.xor_logic("Str", True) is None


def test_XOR_None_2():
    assert logical_gates.xor_logic(True, "Str") is None


def test_XOR_None_3():
    assert logical_gates.xor_logic("Str", "Str") is None

def test_XOR_None_4():
    assert logical_gates.xor_logic(1, 0) is None

