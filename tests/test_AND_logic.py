from logical_gates import logical_gates

"""The following code tests the logical AND-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : False
    True  | False : False
    True  | True  : True """


def test_AND_false_false():
    assert logical_gates.and_logic(False, False) is False


def test_AND_false_true():
    assert logical_gates.and_logic(False, True) is False


def test_AND_true_false():
    assert logical_gates.and_logic(True, False) is False


def test_AND_true_true():
    assert logical_gates.and_logic(True, True) is True


def test_AND_None_1():
    assert logical_gates.and_logic("Str", True) is None


def test_AND_None_2():
    assert logical_gates.and_logic(True, "Str") is None


def test_AND_None_3():
    assert logical_gates.and_logic("Str", "Str") is None


def test_AND_None_4():
    assert logical_gates.and_logic(1, 0) is None
