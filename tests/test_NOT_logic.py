from logical_gates import logical_gates


"""The following code tests the logical NOT-gate.  It's behavior is described in the following truth table
    Bool : Bool
    -----------
    False : True 
    True  : False """


def test_NOT_false():
    assert logical_gates.not_logic(False)  is True


def test_NOT_true():
    assert logical_gates.not_logic(True) is False


def test_NOT_None_1():
    assert logical_gates.not_logic("Str") is None


def test_NOT_None_2():
    assert logical_gates.not_logic(1) is None
