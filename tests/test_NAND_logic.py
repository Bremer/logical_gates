from logical_gates import logical_gates


"""The following code tests the logical NAND-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : True
    True  | False : True
    True  | True  : False """


def test_NAND_false_false():
    assert logical_gates.nand_logic(False, False) is True


def test_NAND_false_true():
    assert logical_gates.nand_logic(False, True) is True


def test_NAND_true_false():
    assert logical_gates.nand_logic(True, False) is True


def test_NAND_true_true():
    assert logical_gates.nand_logic(True, True) is False


def test_NAND_None_1():
    assert logical_gates.nand_logic("Str", True) is None


def test_NAND_None_2():
    assert logical_gates.nand_logic(True, "Str") is None


def test_NAND_None_3():
    assert logical_gates.nand_logic("Str", "Str") is None


def test_NAND_None_4():
    assert logical_gates.nand_logic(1, 0) is None
