from logical_gates import logical_gates


"""The following code represents the logical OR-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : True
    True  | False : True
    True  | True  : True """



def test_OR_false_false():
    assert logical_gates.or_logic(False, False) is False


def test_OR_false_true():
    assert logical_gates.or_logic(False, True) is True


def test_OR_true_false():
    assert logical_gates.or_logic(True, False) is True


def test_OR_true_true():
    assert logical_gates.or_logic(True, True) is True


def test_OR_None_1():
    assert logical_gates.or_logic("Str", True) is None


def test_OR_None_2():
    assert logical_gates.or_logic(True, "Str") is None


def test_OR_None_3():
    assert logical_gates.or_logic("Str", "Str") is None

def test_OR_None_4():
    assert logical_gates.or_logic(1, 0) is None
