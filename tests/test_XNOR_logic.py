from logical_gates import logical_gates

"""The following code represents the logical XNOR-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : False
    True  | False : False
    True  | True  : True """


def test_XNOR_false_false():
    assert logical_gates.xnor_logic(False, False) is True


def test_XNOR_false_true():
    assert logical_gates.xnor_logic(False, True) is False


def test_XNOR_true_false():
    assert logical_gates.xnor_logic(True, False) is False


def test_XNOR_true_true():
    assert logical_gates.xnor_logic(True, True) is True


def test_XNOR_None_1():
    assert logical_gates.xnor_logic("Str", True) is None


def test_XNOR_None_2():
    assert logical_gates.xnor_logic(True, "Str") is None


def test_XNOR_None_3():
    assert logical_gates.xnor_logic("Str", "Str") is None

def test_XNOR_None_4():
    assert logical_gates.xnor_logic(1, 0) is None
