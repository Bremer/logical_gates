from logical_gates import logical_gates


"""The following code tests the logical NOR-gate.  It's behavior is described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : False
    True  | False : False
    True  | True  : False """


def test_NOR_false_false():
    assert logical_gates.nor_logic(False, False) is True


def test_NOR_false_true():
    assert logical_gates.nor_logic(False, True) is False


def test_NOR_true_false():
    assert logical_gates.nor_logic(True, False) is False


def test_NOR_true_true():
    assert logical_gates.nor_logic(True, True) is False


def test_NOR_None_1():
    assert logical_gates.nor_logic("Str", True) is None


def test_NOR_None_2():
    assert logical_gates.nor_logic(True, "Str") is None


def test_NOR_None_3():
    assert logical_gates.nor_logic("Str", "Str") is None


def test_NOR_None_4():
    assert logical_gates.nor_logic(1, 0) is None
