Logical_gates is a small library which enables logical gates through python functions.

Import the file logical_gates.py into your python program.

```
import logical_gates
```
And use the function desired by for example:
```
logical_gates.and_logic(True, True)
```
All functions input parameters shall be booleans. If that is not the case it will return None.

```
$ pytest tests/
========================================== test session starts ===========================================
platform linux -- Python 3.11.4, pytest-7.4.3, pluggy-1.3.0
rootdir: /home/rickard/kod/logical_gates
plugins: cov-4.1.0
collected 52 items
tests/test_AND_logic.py ........                                                                   [ 15%]
tests/test_NAND_logic.py ........                                                                  [ 30%]
tests/test_NOR_logic.py ........                                                                   [ 46%]
tests/test_NOT_logic.py ....                                                                       [ 53%]
tests/test_OR_logic.py ........                                                                    [ 69%]
tests/test_XNOR_logic.py ........                                                                  [ 84%]
tests/test_XOR_logic.py ........                                                                   [100%]

=========================================== 52 passed in 0.03s ===========================================
$ pylint logical_gates.py

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

````

Tested on both Windows and Linux platforms. Unit test framework used is pytest and can be installed with:
```
pip install -U pytest
```
