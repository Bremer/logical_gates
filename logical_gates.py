""" Logical gates described in python code """


def and_logic(bool_a, bool_b):
    """The following code represents the logical AND-gate.  It's behavior is
    described i in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : False
    True  | False : False
    True  | True  : True
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else bool_a and bool_b


def or_logic(bool_a, bool_b):
    """The following code represents the logical OR-gate.  It's behavior is
    described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : True
    True  | False : True
    True  | True  : True
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else bool_a or bool_b


def not_logic(bool_a):
    """The following code represents the logical NOT-gate.  It's behavior is
    described in the following truth table
    Bool : Bool
    -----------
    False : True
    True  : False
    Will return None if wrong datatype is used as input parameters"""
    return None if not boolean(bool_a) else not bool_a


def nand_logic(bool_a, bool_b):
    """The following code represents the logical NAND-gate.  It's behavior is
    described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : True
    True  | False : True
    True  | True  : False
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else not (bool_a and bool_b)


def nor_logic(bool_a, bool_b):
    """The following code represents the logical NOR-gate.  It's behavior is
    described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : False
    True  | False : False
    True  | True  : False
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else not (bool_a or bool_b)


def xor_logic(bool_a, bool_b):
    """The following code represents the logical XOR-gate.  It's behavior is
    described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : False
    False | True  : True
    True  | False : True
    True  | True  : False
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else bool_a is not bool_b


def xnor_logic(bool_a, bool_b):
    """The following code represents the logical XNOR-gate.  It's behavior is
    described in the following truth table
    Bool & Bool   : Bool
    ---------------------
    False | False : True
    False | True  : False
    True  | False : False
    True  | True  : True
    Will return None if wrong datatype is used as input parameters"""
    return None if not booleans(bool_a, bool_b) else bool_a is bool_b


def booleans(bool_a, bool_b):
    """ Check so both input parameters are booleans """
    return isinstance(bool_a, bool) and isinstance(bool_b, bool)


def boolean(bool_a):
    """ Check so input parameter is a boolean. """
    return isinstance(bool_a, bool)
